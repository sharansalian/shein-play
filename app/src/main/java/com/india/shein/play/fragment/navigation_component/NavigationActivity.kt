package com.india.shein.play.fragment.navigation_component

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.india.shein.play.R

class NavigationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)
    }
}