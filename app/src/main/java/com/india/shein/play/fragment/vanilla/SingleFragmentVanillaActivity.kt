package com.india.shein.play.fragment.vanilla

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.india.shein.play.R
import com.india.shein.play.databinding.ActivitySingleFragmentVanillaBinding
import com.india.shein.play.fragment.screens.a.AFragment


class SingleFragmentVanillaActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySingleFragmentVanillaBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySingleFragmentVanillaBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.frameVanilla

        val fragmentA = AFragment()

        val manager: FragmentManager = supportFragmentManager
        val transaction: FragmentTransaction = manager.beginTransaction()
        transaction.add(R.id.frame_vanilla, fragmentA, AFragment.TAG)
        transaction.addToBackStack(null)
        transaction.commit()
    }


}